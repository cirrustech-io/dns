$ORIGIN 2connectintl.com.

$TTL 300
@	IN SOA	ns1.2connectbahrain.com. hostmaster.2connectbahrain.com. (
		2016060801 7200 1200 3600000 7200 )
	172800	IN NS		ns1.2connectbahrain.com.
	172800	IN NS		ns2.2connectbahrain.com.
	172800	IN NS		ns3.2connectbahrain.com.
;		IN MX	10	sm

;@		IN TXT  "v=spf1 mx ip4:80.88.242.27 a:sm.2connectintl.com ~all"

;
; Email Related
smtp		IN CNAME	mail
imap		IN CNAME	mail
pop3		IN CNAME	mail
sm		IN A	80.88.242.27
mail		IN CNAME	mail.2connectbahrain.com.


www		IN A		80.88.242.44

ns1		IN A		80.88.242.4
ns2		IN A		80.88.242.19

@       3600    IN TXT  "MS=ms70111721"
; Office 365 Deployment
2connectintl.com.                          3600    IN MX           0 2connectintl-com.mail.protection.outlook.com.
autodiscover.2connectintl.com.             3600    IN CNAME        autodiscover.outlook.com.
2connectintl.com.                          3600    IN TXT          "v=spf1 include:spf.protection.outlook.com -all"
_sip._tls.2connectintl.com.                3600    IN SRV          100 1 443 sipdir.online.lync.com.
_sipfederationtls._tcp.2connectintl.com.   3600    IN SRV          100 1 5061 sipfed.online.lync.com.
sip.2connectintl.com.                      3600    IN CNAME        sipdir.online.lync.com.
lyncdiscover.2connectintl.com.             3600    IN CNAME        webdir.online.lync.com.
msoid.2connectintl.com.                    3600    IN CNAME        clientconfig.microsoftonline-p.net.
enterpriseenrollment.2connectintl.com.     3600    IN CNAME        enterpriseenrollment.manage.microsoft.com.
enterpriseregistration.2connectintl.com.   3600    IN CNAME	   enterpriseregistration.windows.net.
; End of Office 365 Deployment
;

