$ORIGIN my2connect.com.
$TTL 300
@	IN SOA	ns1.2connectbahrain.com. hostmaster.2connectbahrain.com. (
		2015081801 7200 1200 3600000 7200 )
	172800	IN NS		ns1.2connectbahrain.com.
	172800	IN NS		ns2.2connectbahrain.com.
	172800	IN NS		ns3.2connectbahrain.com.
;		IN MX	10	washer.2connectbahrain.com.
;		IN MX	10	sm.2connectbahrain.com.

@		IN A	80.88.242.10
;@        	IN TXT "v=spf1 mx ip4:80.88.242.36 a:rabbit.2connectbahrain.com -all"
;@		IN TXT	"v=spf1 mx ip4:80.88.242.27 a:sm.2connectbahrain.com ~all"
	
www		IN A	80.88.242.10
;mail	 	IN CNAME mail.2connectbahrain.com.
order		IN A	80.88.242.10
sm		IN A	80.88.242.27
@	3600	IN TXT	"MS=ms92699542"
;
; Office 365 Deployment
my2connect.com.                           3600    IN MX           0 my2connect-com.mail.protection.outlook.com.
autodiscover.my2connect.com.              3600    IN CNAME        autodiscover.outlook.com.
my2connect.com.                           3600    IN TXT          "v=spf1 include:spf.protection.outlook.com -all"
_sip._tls.my2connect.com.                 3600    IN SRV          100 1 443 sipdir.online.lync.com.
_sipfederationtls._tcp.my2connect.com.    3600    IN SRV          100 1 5061 sipfed.online.lync.com.
sip.my2connect.com.                       3600    IN CNAME        sipdir.online.lync.com.
lyncdiscover.my2connect.com.              3600    IN CNAME        webdir.online.lync.com.
msoid.my2connect.com.                     3600    IN CNAME        clientconfig.microsoftonline-p.net.
enterpriseenrollment.my2connect.com.      3600    IN CNAME        enterpriseenrollment.manage.microsoft.com.
enterpriseregistration.my2connect.com.    3600    IN CNAME	  enterpriseregistration.windows.net.

; End of Office 365 Deployment
;
