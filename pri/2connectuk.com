$ORIGIN 2connectuk.com.
$TTL 300
@	IN SOA	ns1.2connectbahrain.com. hostmaster.2connectbahrain.com. (
		2015081802 7200 1200 3600000 7200 )
	172800	IN NS		ns1.2connectbahrain.com.
	172800	IN NS		ns2.2connectbahrain.com.
	172800	IN NS		ns3.2connectbahrain.com.
;		IN MX	10	washer.2connectbahrain.com.
;		IN MX	10	sm.2connectbahrain.com.
;		IN MX	20	hoster.2connectbahrain.com.

;		IN A	80.88.24 IN TXT "v=spf1 a mx ip4:80.88.242.36 ?all"

;@		IN TXT "v=spf1 a mx ip4:80.88.242.36 ?all"
@		IN TXT	"v=spf1 mx ip4:80.88.242.27 a:sm.2connectbahrain.com ~all"

imap		IN CNAME mail.2connectbahrain.com.
smtp		IN CNAME mail.2connectbahrain.com.
mail		IN CNAME mail.2connectbahrain.com.


www             IN A    80.88.242.44
sm		IN A	80.88.242.27
;@	3600	IN TXT	"MS=ms81555933"
@       3600    IN TXT  "MS=ms58450907"
; Office 365 Deployment
2connectuk.com.                          3600    IN MX           0 2connectuk-com.mail.protection.outlook.com.
autodiscover.2connectuk.com.             3600    IN CNAME        autodiscover.outlook.com.
2connectuk.com.                          3600    IN TXT          "v=spf1 include:spf.protection.outlook.com -all"
_sip._tls.2connectuk.com.                3600    IN SRV          100 1 443 sipdir.online.lync.com.
_sipfederationtls._tcp.2connectuk.com.   3600    IN SRV          100 1 5061 sipfed.online.lync.com.
sip.2connectuk.com.                      3600    IN CNAME        sipdir.online.lync.com.
lyncdiscover.2connectuk.com.             3600    IN CNAME        webdir.online.lync.com.
msoid.2connectuk.com.                    3600    IN CNAME        clientconfig.microsoftonline-p.net.
enterpriseenrollment.2connectuk.com.     3600    IN CNAME        enterpriseenrollment.manage.microsoft.com.
enterpriseregistration.2connectuk.com.   3600    IN CNAME	 enterpriseregistration.windows.net.
; End of Office 365 Deployment
;
