$ORIGIN temp.2connectintl.com.

$TTL 300
@       IN SOA  ns1.2connectbahrain.com. hostmaster.2connectbahrain.com. (
                2015062801 7200 1200 3600000 7200 )
        172800  IN NS           ns1.2connectbahrain.com.
        172800  IN NS           ns2.2connectbahrain.com.
        172800  IN NS           ns3.2connectbahrain.com.
                IN MX   10      sm

@               IN TXT  "v=spf1 mx ip4:80.88.242.27 a:sm.2connectintl.com ~all"

;
; Email Related
smtp            IN CNAME        mail
imap            IN CNAME        mail
pop3            IN CNAME        mail
sm              IN A    80.88.242.27
mail            IN CNAME        mail.2connectbahrain.com.


ns1             IN A            80.88.242.4
ns2             IN A            80.88.242.19

