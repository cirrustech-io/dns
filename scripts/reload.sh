#! /bin/bash


host=`hostname -s`

case "$host" in

	dns-master)
		sudo service bind9 reload
		;;
	dns-bh)
		sudo service bind9 reload
		;;
	dns-bh-primary)
		sudo service bind9 reload
		;;
	dns-bh-secondary)
		sudo service bind9 reload
		;;
	dns-bh-02)
		sudo service bind9 reload
		;;
	dns-uk)
		sudo service bind9 reload
		;;
	*)
		echo "Unknown reload script on host [$host]"
		;;
esac

