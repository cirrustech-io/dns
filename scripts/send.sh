#! /bin/bash 


# Do not run as root
if [ `whoami` == "root" ]; then
	echo "Cannot run as root."
	exit 1
fi

# Must have comment to use as commit message
if [ "$1" == "" ]; then
	echo
	echo "ERROR: A commit comment must be used."
	echo
	exit 1
else
	COMMIT_COMMENT=$@
	echo "Commit message specified as follows:"
	echo
	echo "------------------------------------------------------"
	echo $COMMIT_COMMENT
	echo "------------------------------------------------------"
	echo

	read -p "Continue (y/n)?" choice
	case "$choice" in 
		y|Y ) echo "Confirmed."; echo;;
		n|N ) echo "Aborted."; echo; exit 1;;
		* ) echo "Invalid input. Try again."; exit 1;;
	esac
fi
	
SSH="ssh -i /etc/bind/.ssh/dnsupdate -l root"

# {{{ checkconf ()

function checkconf() {
	echo -n "Checking configuration"
	if ! named-checkconf > /dev/null 2>&1
	then
		echo
		echo "Congfiguration check failed, run \"[34mnamed-checkconf[0m\" to see errors"
		echo
		exit 1
	fi
	if ! named-checkconf -z > /dev/null  2>&1 
	then
		echo
		echo "Zone check failed, run \"[34mnamed-checkconf -z[0m\""
		echo
		named-checkconf -z 2>&1 >/dev/null | sed -e 's/^/  /'
		echo
		exit 1
	fi
	echo " [32mPASSED[0m"
}

# }}}
# {{{ send_files()

function my_rsync () {
	rsync -Pcrvre "$SSH" --exclude .git --exclude ".*~" --exclude ".*.sw?" --exclude ".ssh" "$@"
}

function send_files () {
	local host
	local rsync
	
	host=$1
	SRC=/etc
	rsync=my_rsync
	
	$rsync	$SRC/bind/{*.conf*,rndc.key,pri,rfc,scripts,zones}	$host:/etc/bind/
}


# }}}
# {{{ my_save_zone() 

function my_save_zone() {
	local serial
	local zone
	local file
	local sum
	zone="$1"
	file="$2"
	serial="$3"
	sum="$4"
	
	mkdir -p "/etc/bind/serials/$zone"
	if [ -L "/etc/bind/serials/$zone/sum" ] ; then 
		rm -f "/etc/bind/serials/$zone/sum" 2> /dev/null
	fi
	if [ -L "/etc/bind/serials/$zone/serial" ] ; then 
		rm -f "/etc/bind/serials/$zone/serial" 2> /dev/null
	fi
	ln -s "$sum"		"/etc/bind/serials/$zone/sum"
	ln -s "$serial"		"/etc/bind/serials/$zone/serial"

}

# }}}
# {{{ check_serial()

function check_serial () {
	local serial
	local zone
	local file
	local sum
	local s_sum
	local f_serial
	error=0
	touch /etc/bind/serials/--zone-errors--
	rm /etc/bind/serials/--zone-errors--
	named-checkconf -z | sed -e 's@^zone \(.*\)/IN: loaded serial \([0-9]*\)$@\2 \1@' | sort | uniq | while read serial zone
	do
		file="$( egrep "zone[ 	]*\"$zone\"" /etc/bind/zones -r -A 10 | grep file | head -n 1 | sed -e 's/.*file[ 	]\+"\([^"]*\)".*/\1/' )"
#		echo "$zone [$file]"
#		named-checkzone "$zone" "$file"
		sum=`md5sum -b "$file" | cut -c 1-32`
		if [ ! -L "/etc/bind/serials/$zone/sum"  ]; then
			my_save_zone "$zone" "$file" "$serial" "$sum"
			continue
		fi
		if [ ! -L "/etc/bind/serials/$zone/serial"  ]; then
			my_save_zone "$zone" "$file" "$serial" "$sum"
			continue
		fi
		s_sum=`readlink "/etc/bind/serials/$zone/sum"`
		s_serial=`readlink "/etc/bind/serials/$zone/serial"`
		[ "x$s_sum" == "x$sum" ] && continue
		touch /etc/bind/serials/--zone-modified--

		if [ "$serial" -gt "$s_serial" ]; then
			my_save_zone "$zone" "$file" "$serial" "$sum"
			continue
		fi
		echo "Error with zone \"$zone\" in file \"$file\""
		touch /etc/bind/serials/--zone-errors--
		if [ "$serial" = "$s_serial" ]; then
			echo "  Zone file modified but the serial didn't change"
			echo "  Please update serial !"
			echo 
			continue
		fi
		echo 

	done
	if [ -e /etc/bind/serials/--zone-errors-- ]; then
		echo "Found errors, not syncing"
		rm /etc/bind/serials/--zone-errors--
		exit 1
	fi
}

# }}}

checkconf
check_serial

# Send configuration to servers and reload bind process
for server in dns-bh dns-uk dns-bh-02
do
	send_files	$server
	echo
	echo
	echo "Reloading DNS Server on host - $server"
	echo
	$SSH	$server		/etc/bind/scripts/reload.sh
done

# Commit and push changes to Git repository
echo
cd /etc/bind/
GIT_REPO=`git remote -v | grep push | sed -r 's/[a-zA-Z]*\s*(.*)*\s*\(.*\)/\1/g'`
echo "Committing and pushing to Git repository at $GIT_REPO"
git add .
git commit -m "$COMMIT_COMMENT"
git push --all origin
echo
echo
#
#

